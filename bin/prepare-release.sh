#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/build.yml
cat ./src/gitlab-ci/build.yml >> ./artifacts/release/build.yml
