# Documentation System

Client and Developer Guides

* Written in [Markdown](https://www.markdownguide.org/basic-syntax/)
* Rendered and browsed in GitLab
* Automated Testing Tools (Run by `bin/test.sh`):
  * [markdown-link-check](https://github.com/tcort/markdown-link-check#readme)
  * [markdownlint](https://github.com/DavidAnson/markdownlint)
  * [markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2)
